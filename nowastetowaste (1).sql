-- phpMyAdmin SQL Dump
-- version 4.0.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jun 09, 2018 at 04:23 PM
-- Server version: 5.6.21
-- PHP Version: 5.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `nowastetowaste`
--

-- --------------------------------------------------------

--
-- Table structure for table `bankaccounts`
--

CREATE TABLE IF NOT EXISTS `bankaccounts` (
  `BankAccountID` int(11) NOT NULL AUTO_INCREMENT,
  `BankAccountNumber` varchar(60) CHARACTER SET utf8 NOT NULL,
  `ProductTypeID` int(11) NOT NULL,
  `OriginalAmount` float NOT NULL,
  `AmountRemaining` float NOT NULL,
  PRIMARY KEY (`BankAccountID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `bankaccounts`
--

INSERT INTO `bankaccounts` (`BankAccountID`, `BankAccountNumber`, `ProductTypeID`, `OriginalAmount`, `AmountRemaining`) VALUES
(1, 'NLABNA0455900570', 1, 55555, 55535),
(2, 'NLABNA0435306057', 1, 108, -5492),
(3, 'NLABNA0539322518', 1, 2500, -554177),
(4, 'NLABNA0919199390', 1, 2500, 0),
(5, 'NLABNA0815453539', 1, 2500, 2495),
(6, 'NLABNA0195377128', 1, 2500, 0),
(7, 'NLABNA0512913728', 1, 2500, 0),
(8, 'NLABNA0617207551', 1, 2500, 2475),
(9, 'NLABNA0808824030', 1, 2500, 0);

-- --------------------------------------------------------

--
-- Table structure for table `buyers`
--

CREATE TABLE IF NOT EXISTS `buyers` (
  `BuyerID` int(11) NOT NULL AUTO_INCREMENT,
  `BuyerName` varchar(255) CHARACTER SET utf8 NOT NULL,
  `BuyerEmail` varchar(255) CHARACTER SET utf8 NOT NULL,
  `BuyerBankAccount` varchar(60) NOT NULL,
  `Blocked` tinyint(1) NOT NULL,
  PRIMARY KEY (`BuyerID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `contracts`
--

CREATE TABLE IF NOT EXISTS `contracts` (
  `ContractID` int(11) NOT NULL AUTO_INCREMENT,
  `BuyerName` varchar(30) NOT NULL,
  `SellerName` varchar(30) NOT NULL,
  `ContractDescription` varchar(255) CHARACTER SET utf8 NOT NULL,
  `ContractStartDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ContractStatus` tinyint(1) NOT NULL,
  `BuyerID` int(11) DEFAULT NULL,
  `SellerID` int(11) DEFAULT NULL,
  `SellerOrderReference` varchar(30) DEFAULT NULL,
  `BankAccountID` int(11) DEFAULT NULL,
  `InsuranceID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ContractID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `contracts`
--

INSERT INTO `contracts` (`ContractID`, `BuyerName`, `SellerName`, `ContractDescription`, `ContractStartDateTime`, `ContractStatus`, `BuyerID`, `SellerID`, `SellerOrderReference`, `BankAccountID`, `InsuranceID`) VALUES
(1, '', '', 'Bricks purchase contract', '2018-06-09 00:00:00', 0, 2, 2, 'MGM_428887', 1, 111222),
(2, '', '', 'bricks', '2018-06-09 00:00:00', 0, 9, 2, 'MGM_549755', 2, 111222),
(3, '', '', 'Bricks,Wood', '2018-06-09 00:00:00', 0, 20, 2, '12543', 3, 111222),
(4, '', '', 'Bricks,Wood', '2018-06-09 00:00:00', 0, 20, 2, '12543', 4, 111222),
(5, '', '', 'Bricks,Wood', '2018-06-09 00:00:00', 0, 2, 26, '12543', 5, 111222),
(6, '', '', 'something', '2018-06-09 00:00:00', 0, 33, 26, '12543', 6, 111222),
(7, 'pavan', 'A-Traders', 'Bricks,Wood', '2018-06-09 00:00:00', 0, 26, 34, '12543', 8, 111222),
(8, 'sai', 'A-Traders', 'Bricks,Wood', '2018-06-09 00:00:00', 0, 26, 34, '12543', 9, 111222);

-- --------------------------------------------------------

--
-- Table structure for table `insurances`
--

CREATE TABLE IF NOT EXISTS `insurances` (
  `InsuranceID` int(11) NOT NULL AUTO_INCREMENT,
  `InsuranceContractNumber` varchar(60) CHARACTER SET utf8 NOT NULL,
  `ProductTypeID` int(11) NOT NULL,
  PRIMARY KEY (`InsuranceID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE IF NOT EXISTS `invoices` (
  `InvoiceID` int(11) NOT NULL AUTO_INCREMENT,
  `InvoiceSentDate` date NOT NULL,
  `InvoiceStatus` tinyint(1) NOT NULL,
  `InvoiceDueDate` date NOT NULL,
  PRIMARY KEY (`InvoiceID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `producttypes`
--

CREATE TABLE IF NOT EXISTS `producttypes` (
  `ProductTypeID` int(11) NOT NULL AUTO_INCREMENT,
  `ProductName` varchar(255) NOT NULL,
  `ProductURL` text CHARACTER SET ascii,
  `ProductTerms` text CHARACTER SET ascii,
  PRIMARY KEY (`ProductTypeID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `producttypes`
--

INSERT INTO `producttypes` (`ProductTypeID`, `ProductName`, `ProductURL`, `ProductTerms`) VALUES
(1, 'Blocked Bank Account', 'https://www.abnamro.nl/nl/zakelijk/betalen/g-rekening/index.html', 'https://www.abnamro.nl/nl/images/Content/000_Sitewide_gedeeld/Documenten/pdf_Algemene_Voorwaarden_ABN_AMRO_Bank_NV.pdf'),
(2, 'Damage Insurance', 'https://www.abnamro.nl/nl/zakelijk/verzekeren/bedrijfspolis/index.html', 'https://www.abnamro.nl/nl/images/Content/022_Zakelijk_nieuwe_structuur/000_Gedeelde_documenten/pdf_aansprakelijkheidsverzekering_voorwaarden.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `sellers`
--

CREATE TABLE IF NOT EXISTS `sellers` (
  `SellerID` int(11) NOT NULL AUTO_INCREMENT,
  `SellerName` varchar(255) CHARACTER SET utf8 NOT NULL,
  `SellerEmail` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Address` varchar(100) NOT NULL,
  `ZipCode` varchar(7) NOT NULL,
  `City` varchar(40) NOT NULL,
  `Lat` decimal(10,8) NOT NULL,
  `Lng` decimal(11,8) NOT NULL,
  `SellerBankAccount` varchar(60) NOT NULL,
  `Blocked` tinyint(1) NOT NULL,
  PRIMARY KEY (`SellerID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE IF NOT EXISTS `transactions` (
  `TransactionID` int(11) NOT NULL AUTO_INCREMENT,
  `TransactionDateTime` datetime DEFAULT NULL,
  `InitiatedByBuyerID` int(11) NOT NULL,
  `ContractID` int(11) NOT NULL,
  `TransactionAmount` float NOT NULL,
  `TransactionType` varchar(25) NOT NULL,
  PRIMARY KEY (`TransactionID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `UserID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `HashedPassword` varchar(40) CHARACTER SET ascii NOT NULL,
  `UserType` tinyint(1) NOT NULL,
  `Address` varchar(100) NOT NULL,
  `ZipCode` varchar(7) NOT NULL,
  `City` varchar(40) NOT NULL,
  `Lat` decimal(10,8) NOT NULL,
  `Lng` decimal(11,8) NOT NULL,
  `LogoURL` varchar(100) NOT NULL,
  `BankAccount` varchar(60) DEFAULT NULL,
  `Blocked` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`UserID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=35 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`UserID`, `Name`, `Email`, `HashedPassword`, `UserType`, `Address`, `ZipCode`, `City`, `Lat`, `Lng`, `LogoURL`, `BankAccount`, `Blocked`) VALUES
(2, 'MAGMA', 'ranjanpandey2006@gmail.com', 'magma', 1, 'Bijlmer Arena', '1102 BS', 'Amsterdam', 0.00000000, 0.00000000, 'http://www.green-pay.nl/Magma/Magma-logo.png', 'NLABNA0960275106', 0),
(4, 'niranjan', 'niranjan@gmail.com', 'niranjan', 1, 'niranjan', '6789', 'niranjan', 0.00000000, 0.00000000, 'www.google.com', 'NLABNA0396522031', 0),
(5, '', '', '', 1, '', '', '', 0.00000000, 0.00000000, 'www.google.com', 'NLABNA0176693580', 0),
(6, 'Alisha', 'alisha@gmail.com', 'alisha', 0, 'Amsterdam', '1002', 'Amsterdam', 0.00000000, 0.00000000, 'www.google.com', NULL, 0),
(7, 'niranjan', 'niranjan@tcs.com', 'niranjan', 1, 'test', '6767', 'test', 0.00000000, 0.00000000, 'www.google.com', 'NLABNA0390687468', 0),
(8, 'niranjan', 'niranjan@niranjan.com', 'niranjan', 1, 'test', '5464', 'test', 0.00000000, 0.00000000, 'www.google.com', NULL, 0),
(9, 'Bob-The-Builder', 'karel.lammers@gmail.com', 'Welcome@123', 2, 'Terwijde', '3547 DE', 'Utrecht', 16.00000000, 67.00000000, '', 'NLABNA0987654321', 0),
(10, 'niranjan', 'niranjan', 'niranjan', 1, 'niranjan', 'rt5656', 'niranjan', 0.00000000, 0.00000000, 'www.google.com', NULL, 0),
(11, 'niranjan', 'niranjan', 'Nira@123', 1, 'niranjan', '5656', 'niranjan', 0.00000000, 0.00000000, 'www.google.com', NULL, 0),
(12, 'niranjan', 'niranjan', 'Nira@123', 1, 'nirabjan', '5656', 'niranjan', 0.00000000, 0.00000000, 'www.google.com', NULL, 0),
(13, 'niranjan', 'niranjan', 'Nira@123', 1, 'niranjan', '23123', 'niranjan', 0.00000000, 0.00000000, 'www.google.com', NULL, 0),
(14, '', 'niranjan', 'Nira@123', 1, '', '', '', 0.00000000, 0.00000000, 'www.google.com', NULL, 0),
(15, 'niranan', 'niranjan', 'niranjan', 1, 'niranajn', '4545', 'niranjan', 0.00000000, 0.00000000, 'www.google.com', NULL, 0),
(16, 'niranjan', 'niranjan', 'niranjan', 1, 'test', '5656', 'niranjan', 0.00000000, 0.00000000, 'www.google.com', NULL, 0),
(17, 'niranjan', 'niranjan', 'Nira@123', 1, 'niranjan', '6846XL', 'niranjan', 0.00000000, 0.00000000, 'www.google.com', NULL, 0),
(18, 'niranjan', 'niranjan', 'niranjan', 1, 'niranjan', '5656', 'niranjan', 0.00000000, 0.00000000, 'www.google.com', NULL, 0),
(19, 'niranjan', 'niranjan', 'Nira@123', 1, 'niranjan', '5656', 'niranjan', 0.00000000, 0.00000000, 'www.google.com', NULL, 0),
(20, 'pavan', 'pavan1062.k@gmail.com', 'Welcome@123', 2, 'Terwijde', '3547 DE', 'Utrecht', 16.00000000, 67.00000000, '', 'NLABNA0987654321', 0),
(24, 'test', 'test@test.nl', 'test', 2, 'test', '2537', 'test', 0.00000000, 0.00000000, 'www.google.com', NULL, 0),
(25, 'niranjan', 'niranjan@nn.nl', 'niranjan', 1, 'test', '3434', 'test', 0.00000000, 0.00000000, 'www.google.com', 'NLABNA0358268136', 0),
(26, 'paaca', 'pavansai.biit@gmail.com', '1234', 1, 'sassas', '1234', 'sa', 0.00000000, 0.00000000, 'www.google.com', NULL, 0),
(27, 'Alisha', 'alisha@gmail.com', 'alisha', 0, 'Amsterdam', '1002', 'Amsterdam', 0.00000000, 0.00000000, 'www.google.com', NULL, 0),
(28, 'niranjan', 'niran@sogeti.com', 'niranjan', 1, 'test', '4545', 'test', 0.00000000, 0.00000000, 'www.google.com', 'NLABNA0728427799', 0),
(29, 'test', 'test@test.nl', 'test', 2, 'test', '343434', 'test', 0.00000000, 0.00000000, 'www.google.com', NULL, 0),
(30, 'test', 'test@test.nl', 'test', 1, 'test', '23123', 'test', 0.00000000, 0.00000000, 'www.google.com', NULL, 0),
(31, 'test', 'test@test.nl', 'test', 0, 'test', '3434', 'test', 0.00000000, 0.00000000, 'www.google.com', NULL, 0),
(32, 'niranjan', 'niranjan@nl.nl', 'niranjan', 2, 'test', '4353', 'test', 0.00000000, 0.00000000, 'www.google.com', NULL, 0),
(33, 'alisha', 'alishapatel2006@gmail.com', 'Welcome@123', 2, 'Terwijde', '3547 DE', 'Utrecht', 16.00000000, 67.00000000, '', 'NLABNA0987654321', 0),
(34, 'A-Traders', 'pavan1062.k@gmail.com', '1234', 1, 'kjskd', '1234', 'ccis', 0.00000000, 0.00000000, 'www.google.com', NULL, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
