-- phpMyAdmin SQL Dump
-- version 4.0.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jun 09, 2018 at 05:32 AM
-- Server version: 5.6.21
-- PHP Version: 5.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `nowastetowaste`
--

-- --------------------------------------------------------

--
-- Table structure for table `bankaccounts`
--

CREATE TABLE IF NOT EXISTS `bankaccounts` (
  `BankAccountID` int(11) NOT NULL AUTO_INCREMENT,
  `BankAccountNumber` varchar(60) CHARACTER SET utf8 NOT NULL,
  `ProductTypeID` int(11) NOT NULL,
  `OriginalAmount` float NOT NULL,
  `AmountRemaining` float NOT NULL,
  PRIMARY KEY (`BankAccountID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=42 ;

-- --------------------------------------------------------

--
-- Table structure for table `buyers`
--

CREATE TABLE IF NOT EXISTS `buyers` (
  `BuyerID` int(11) NOT NULL AUTO_INCREMENT,
  `BuyerName` varchar(255) CHARACTER SET utf8 NOT NULL,
  `BuyerEmail` varchar(255) CHARACTER SET utf8 NOT NULL,
  `BuyerBankAccount` varchar(60) NOT NULL,
  `Blocked` tinyint(1) NOT NULL,
  PRIMARY KEY (`BuyerID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `buyers`
--

INSERT INTO `buyers` (`BuyerID`, `BuyerName`, `BuyerEmail`, `BuyerBankAccount`, `Blocked`) VALUES
(1, 'Another month another house', 'karel.lammers@gmail.com', 'NL46ABNA345009456', 0),
(2, 'SME House Builder Amsterdam', 'karel.lammers@endory.nl', 'NL44BUNQ026354129', 0);

-- --------------------------------------------------------

--
-- Table structure for table `contracts`
--

CREATE TABLE IF NOT EXISTS `contracts` (
  `ContractID` int(11) NOT NULL AUTO_INCREMENT,
  `ContractDescription` varchar(255) CHARACTER SET utf8 NOT NULL,
  `ContractStartDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ContractStatus` tinyint(1) NOT NULL,
  `BuyerID` int(11) DEFAULT NULL,
  `SellerID` int(11) DEFAULT NULL,
  `SellerOrderReference` varchar(30) DEFAULT NULL,
  `BankAccountID` int(11) DEFAULT NULL,
  `InsuranceID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ContractID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

--
-- Dumping data for table `contracts`
--

INSERT INTO `contracts` (`ContractID`, `ContractDescription`, `ContractStartDateTime`, `ContractStatus`, `BuyerID`, `SellerID`, `SellerOrderReference`, `BankAccountID`, `InsuranceID`) VALUES
(1, 'Canalstreet 23', '2018-06-08 00:00:00', 0, 3, 2, 'Order Nr 842/5623', 1, 1),
(2, 'Foppingadreef 22', '2018-06-08 00:00:00', 1, 4, 1, NULL, 2, 2),
(3, 'Amstel 256', '2018-06-08 00:00:00', 1, NULL, 1, 'test reference', NULL, NULL),
(4, 'Amstel 256', '2018-06-08 00:00:00', 2, NULL, NULL, 'test reference', NULL, NULL),
(5, 'Amstel 256', '2018-06-08 00:00:00', 3, NULL, 1, 'test reference', NULL, NULL),
(6, 'Amstel 256', '2018-06-08 00:00:00', 0, NULL, NULL, 'test reference', NULL, NULL),
(7, 'Amstel 256', '2018-06-08 00:00:00', 1, NULL, NULL, 'test reference', NULL, NULL),
(8, 'Amsterdam 789', '2018-06-08 00:00:00', 0, NULL, NULL, 'ams test reference', NULL, NULL),
(9, 'Amstel 256 - bb', '2018-06-08 00:00:00', 0, 3, 1, 'test reference 1122', 7, 111222),
(10, 'Beyond Banking', '2018-06-08 00:00:00', 0, 8, 1, 'Email testing', 11, 111222),
(11, 'Beyond Banking', '2018-06-08 00:00:00', 0, 8, 1, 'Email testing', 14, 111222),
(12, 'Beyond Banking', '2018-06-08 00:00:00', 0, 8, 3, 'Email testing', 15, 111222),
(13, 'Beyond Banking', '2018-06-08 00:00:00', 0, 8, 3, 'Email testing', 19, 111222),
(14, 'Beyond Banking', '2018-06-08 00:00:00', 0, 8, 3, 'Email testing', 23, 111222),
(15, 'Beyond Banking', '2018-06-08 00:00:00', 0, 8, 3, 'Email testing', NULL, 111222),
(16, 'Beyond Banking', '2018-06-08 00:00:00', 0, 8, 3, 'Email testing', NULL, 111222),
(17, 'Beyond Banking', '2018-06-08 00:00:00', 0, 8, 3, 'Email testing', NULL, 111222),
(18, 'Beyond Banking', '2018-06-08 00:00:00', 0, 8, 3, 'Email testing', NULL, 111222),
(19, 'Beyond Banking', '2018-06-08 00:00:00', 0, 8, 3, 'Email testing', NULL, 111222),
(20, 'Beyond Banking', '2018-06-08 00:00:00', 0, 8, 3, 'Email testing', NULL, 111222),
(21, 'Beyond Banking', '2018-06-08 00:00:00', 0, 8, 3, 'Email testing', NULL, 111222),
(22, 'Beyond Banking', '2018-06-08 00:00:00', 0, 8, 3, 'Email testing', NULL, 111222),
(23, 'Beyond Banking', '2018-06-08 00:00:00', 0, 8, 3, 'Email testing', NULL, 111222),
(24, 'Beyond Banking', '2018-06-08 00:00:00', 0, 8, 3, 'Email testing', 33, 111222),
(25, 'Beyond Banking', '2018-06-08 00:00:00', 0, 8, 3, 'Email testing', 34, 111222),
(26, 'Beyond Banking', '2018-06-08 00:00:00', 0, 8, 2, 'Email testing', 35, 111222),
(27, 'Beyond Banking', '2018-06-08 00:00:00', 0, 8, 2, 'Email testing', 36, 111222),
(28, 'Beyond Banking', '2018-06-08 00:00:00', 0, 8, 2, 'Email testing', 37, 111222),
(29, 'Beyond Banking', '2018-06-08 00:00:00', 0, 8, 2, 'Email testing', 38, 111222),
(30, 'Beyond Banking', '2018-06-08 00:00:00', 0, 8, 2, 'Email testing', 39, 111222),
(31, 'Beyond Banking', '2018-06-08 00:00:00', 0, 8, 2, 'Email testing', 40, 111222),
(32, 'Beyond Banking', '2018-06-09 00:00:00', 0, 8, 2, 'Email testing', 41, 111222);

-- --------------------------------------------------------

--
-- Table structure for table `insurances`
--

CREATE TABLE IF NOT EXISTS `insurances` (
  `InsuranceID` int(11) NOT NULL AUTO_INCREMENT,
  `InsuranceContractNumber` varchar(60) CHARACTER SET utf8 NOT NULL,
  `ProductTypeID` int(11) NOT NULL,
  PRIMARY KEY (`InsuranceID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `insurances`
--

INSERT INTO `insurances` (`InsuranceID`, `InsuranceContractNumber`, `ProductTypeID`) VALUES
(1, 'NL22ABNA0345672959', 2),
(2, 'NL55ABNA0922309871', 2);

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE IF NOT EXISTS `invoices` (
  `InvoiceID` int(11) NOT NULL AUTO_INCREMENT,
  `InvoiceSentDate` date NOT NULL,
  `InvoiceStatus` tinyint(1) NOT NULL,
  `InvoiceDueDate` date NOT NULL,
  PRIMARY KEY (`InvoiceID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `producttypes`
--

CREATE TABLE IF NOT EXISTS `producttypes` (
  `ProductTypeID` int(11) NOT NULL AUTO_INCREMENT,
  `ProductName` varchar(255) NOT NULL,
  `ProductURL` text CHARACTER SET ascii,
  `ProductTerms` text CHARACTER SET ascii,
  PRIMARY KEY (`ProductTypeID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `producttypes`
--

INSERT INTO `producttypes` (`ProductTypeID`, `ProductName`, `ProductURL`, `ProductTerms`) VALUES
(1, 'Blocked Bank Account', 'https://www.abnamro.nl/nl/zakelijk/betalen/g-rekening/index.html', 'https://www.abnamro.nl/nl/images/Content/000_Sitewide_gedeeld/Documenten/pdf_Algemene_Voorwaarden_ABN_AMRO_Bank_NV.pdf'),
(2, 'Damage Insurance', 'https://www.abnamro.nl/nl/zakelijk/verzekeren/bedrijfspolis/index.html', 'https://www.abnamro.nl/nl/images/Content/022_Zakelijk_nieuwe_structuur/000_Gedeelde_documenten/pdf_aansprakelijkheidsverzekering_voorwaarden.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `sellers`
--

CREATE TABLE IF NOT EXISTS `sellers` (
  `SellerID` int(11) NOT NULL AUTO_INCREMENT,
  `SellerName` varchar(255) CHARACTER SET utf8 NOT NULL,
  `SellerEmail` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Address` varchar(100) NOT NULL,
  `ZipCode` varchar(7) NOT NULL,
  `City` varchar(40) NOT NULL,
  `Lat` decimal(10,8) NOT NULL,
  `Lng` decimal(11,8) NOT NULL,
  `SellerBankAccount` varchar(60) NOT NULL,
  `Blocked` tinyint(1) NOT NULL,
  PRIMARY KEY (`SellerID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `sellers`
--

INSERT INTO `sellers` (`SellerID`, `SellerName`, `SellerEmail`, `Address`, `ZipCode`, `City`, `Lat`, `Lng`, `SellerBankAccount`, `Blocked`) VALUES
(1, 'Praxosi', 'karel.lammers@nl.abnamro.com', 'Builderslane 25', '1102 XS', 'Amsterdam', 0.00000000, 0.00000000, 'NL34INGB354662134', 0),
(2, 'MAGMA', 'info@endory.nl', 'Woodstreet 6', '3513 AB', 'Utrecht', 0.00000000, 0.00000000, 'NL55RABO756673409', 0);

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE IF NOT EXISTS `transactions` (
  `TransactionID` int(11) NOT NULL AUTO_INCREMENT,
  `TransactionDateTime` datetime DEFAULT NULL,
  `InitiatedByBuyerID` int(11) NOT NULL,
  `ContractID` int(11) NOT NULL,
  `TransactionAmount` float NOT NULL,
  `TransactionType` varchar(25) NOT NULL,
  PRIMARY KEY (`TransactionID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`TransactionID`, `TransactionDateTime`, `InitiatedByBuyerID`, `ContractID`, `TransactionAmount`, `TransactionType`) VALUES
(1, '2018-05-04 10:22:07', 1, 1, 50000, 'Deposit'),
(2, '2018-05-10 11:13:47', 1, 1, -6000, 'Received materials'),
(3, '2018-05-21 09:26:33', 1, 1, -4000, 'Received materials'),
(4, '2018-05-17 15:05:06', 2, 2, 20000, 'Deposit'),
(5, '2018-05-19 14:02:57', 2, 2, -5000, 'Received materials'),
(6, '2018-05-28 13:05:21', 1, 1, 2000, 'Deposit');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `UserID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `HashedPassword` varchar(40) CHARACTER SET ascii NOT NULL,
  `UserType` tinyint(1) NOT NULL,
  `Address` varchar(100) NOT NULL,
  `ZipCode` varchar(7) NOT NULL,
  `City` varchar(40) NOT NULL,
  `Lat` decimal(10,8) NOT NULL,
  `Lng` decimal(11,8) NOT NULL,
  `LogoURL` varchar(100) NOT NULL,
  `BankAccount` varchar(60) DEFAULT NULL,
  `Blocked` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`UserID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`UserID`, `Name`, `Email`, `HashedPassword`, `UserType`, `Address`, `ZipCode`, `City`, `Lat`, `Lng`, `LogoURL`, `BankAccount`, `Blocked`) VALUES
(1, 'Praxia', 'karel.lammers@nl.abnamro.com', '', 1, 'Builderslane 25', '1102 XS', 'Amsterdam', 52.32368000, 4.94609200, '', 'NL34INGB354662134', 0),
(2, 'MAGMA', 'karel.lammers@gmail.com', '', 1, 'Woodstreet 6', '3513 AB', 'Utrecht', 52.09662500, 5.10716900, '', 'NL55RABO756673409', 0),
(3, 'Another month another house', 'karel.lammers@gmail.com', '', 2, 'Makestreet 5', '5643 AH', 'Eindhoven', 51.42206700, 5.49446800, '', 'NL46ABNA345009456', 0),
(4, 'Bob The Builder Amsterdam', 'karel.lammers@endory.nl', '', 2, 'Buildstreet 93', '1102 BA', 'Amsterdam', 52.31871600, 4.95741900, '', 'NL44BUNQ026354129', 0),
(5, 'GreenPayAdmin', 'info@green-pay.nl', '', 0, '', '', '', 0.00000000, 0.00000000, '', '', 0),
(6, 'Alisha', 'alisha@gmail.com', 'alisha', 0, 'Amsterdam', '1002', 'Amsterdam', 0.00000000, 0.00000000, 'www.google.com', NULL, 0),
(8, 'Ranjan', 'ranjanpandey2006@gmail.com', 'ranjan', 0, 'Amsterdam', '1007', 'Amsterdam', 0.00000000, 0.00000000, 'www.google.nl', NULL, 0),
(12, 'Alisha', 'alisha@gmail.com', 'alisha', 0, 'Amsterdam', '1002', 'Amsterdam', 0.00000000, 0.00000000, 'www.google.com', NULL, 0),
(11, 'Alisha', 'alisha@gmail.com', 'alisha', 0, 'Amsterdam', '1002', 'Amsterdam', 0.00000000, 0.00000000, 'www.google.com', NULL, 0),
(13, 'Alisha', 'alisha@gmail.com', 'alisha', 0, 'Amsterdam', '1002', 'Amsterdam', 0.00000000, 0.00000000, 'www.google.com', NULL, 0),
(14, 'Alisha', 'alisha@gmail.com', 'alisha', 0, 'Amsterdam', '1002', 'Amsterdam', 0.00000000, 0.00000000, 'www.google.com', NULL, 0),
(15, 'Alisha', 'alisha@gmail.com', 'alisha', 0, 'Amsterdam', '1002', 'Amsterdam', 0.00000000, 0.00000000, 'www.google.com', NULL, 0),
(16, 'Alisha', 'alisha@gmail.com', 'alisha', 0, 'Amsterdam', '1002', 'Amsterdam', 0.00000000, 0.00000000, 'www.google.com', NULL, 0),
(17, 'Niranjan Rath', 'niranjan@test.com', 'Nira@123', 1, 'test', '2312DR', 'Arnhem', 0.00000000, 0.00000000, 'www.google.com', NULL, 0),
(18, 'Niranjan Rath', 'doddegras@gmail.com', 'Nira@123', 1, 'foppinggadreef', '6767CM', 'Amsterdam', 0.00000000, 0.00000000, 'www.google.com', NULL, 0),
(19, 'Niranjan Rath', 'doddegras@gmail.com', 'niranjan', 1, 'foppinggadreef', '6767CM', 'Amsterdam', 0.00000000, 0.00000000, 'www.google.com', NULL, 0),
(20, 'Niranjan Rath', 'doddegras@gmail.com', 'niranjan', 1, 'Abbagail, Abbagail', '456789', 'aa', 0.00000000, 0.00000000, 'www.google.com', NULL, 0),
(21, 'Niranjan Rath', 'doddegras@gmail.com', 'Nira@123', 1, 'Abbagail, Abbagail', '6846AS', 'Abbagail', 0.00000000, 0.00000000, 'www.google.com', NULL, 0),
(22, 'Niranjan Rath', 'doddegras@gmail.com', 'Nira@123', 1, 'Abbagail, Abbagail', '43333', 'Arnhem', 0.00000000, 0.00000000, 'www.google.com', NULL, 0),
(23, 'Ranjan', 'ranjanpandey2006@gmail.com', 'ranjan', 0, 'Amsterdam', '1007', 'Amsterdam', 0.00000000, 0.00000000, 'www.google.nl', NULL, 0),
(24, 'Ranjan', 'ranjanpandey2006@gmail.com', 'ranjan', 0, 'Amsterdam', '1007', 'Amsterdam', 0.00000000, 0.00000000, 'www.google.nl', NULL, 0),
(25, 'Ranjan', 'ranjanpandey2006@gmail.com', 'ranjan', 0, 'Amsterdam', '1007', 'Amsterdam', 0.00000000, 0.00000000, 'www.google.nl', NULL, 0),
(26, 'Ranjan', 'ranjanpandey2006@gmail.com', 'ranjan', 1, 'Amsterdam', '1002BS', 'Amsterdam', 0.00000000, 0.00000000, 'www.google.com', NULL, 0),
(27, 'Ranjan', 'ranjanpandey2006@gmail.com', 'ranjan', 0, 'Amsterdam', '1007', 'Amsterdam', 0.00000000, 0.00000000, 'www.google.nl', NULL, 0),
(28, 'Ranjan', 'ranjanpandey2006@gmail.com', 'ranjan', 0, 'Amsterdam', '1007', 'Amsterdam', 0.00000000, 0.00000000, 'www.google.nl', NULL, 0),
(29, 'Seller 1', 'ranjanpandey2006@gmail.com', 'seller1', 1, 'Seller1 Amsterdam', '18652BS', 'Amsterdam', 0.00000000, 0.00000000, 'www.google.com', 'NLABNA0532039293', 0),
(30, 'Ranjan', 'ranjanpandey2006@gmail.com', 'ranjan', 0, 'Amsterdam', '1007', 'Amsterdam', 0.00000000, 0.00000000, 'www.google.nl', NULL, 0),
(31, '', '', '', 1, '', '', '', 0.00000000, 0.00000000, 'www.google.com', NULL, 0),
(32, 'pavan', 'pavan@sai.com', 'saai', 1, 'jskdjlsk ksdjlksds', '1132', 'dsd', 0.00000000, 0.00000000, 'www.google.com', NULL, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
