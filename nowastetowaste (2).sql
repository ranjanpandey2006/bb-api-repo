-- phpMyAdmin SQL Dump
-- version 4.0.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jun 09, 2018 at 04:23 PM
-- Server version: 5.6.21
-- PHP Version: 5.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `nowastetowaste`
--

-- --------------------------------------------------------

--
-- Table structure for table `bankaccounts`
--

CREATE TABLE IF NOT EXISTS `bankaccounts` (
  `BankAccountID` int(11) NOT NULL AUTO_INCREMENT,
  `BankAccountNumber` varchar(60) CHARACTER SET utf8 NOT NULL,
  `ProductTypeID` int(11) NOT NULL,
  `OriginalAmount` float NOT NULL,
  `AmountRemaining` float NOT NULL,
  PRIMARY KEY (`BankAccountID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

--
-- Table structure for table `buyers`
--

CREATE TABLE IF NOT EXISTS `buyers` (
  `BuyerID` int(11) NOT NULL AUTO_INCREMENT,
  `BuyerName` varchar(255) CHARACTER SET utf8 NOT NULL,
  `BuyerEmail` varchar(255) CHARACTER SET utf8 NOT NULL,
  `BuyerBankAccount` varchar(60) NOT NULL,
  `Blocked` tinyint(1) NOT NULL,
  PRIMARY KEY (`BuyerID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `contracts`
--

CREATE TABLE IF NOT EXISTS `contracts` (
  `ContractID` int(11) NOT NULL AUTO_INCREMENT,
  `BuyerName` varchar(30) NOT NULL,
  `SellerName` varchar(30) NOT NULL,
  `ContractDescription` varchar(255) CHARACTER SET utf8 NOT NULL,
  `ContractStartDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ContractStatus` tinyint(1) NOT NULL,
  `BuyerID` int(11) DEFAULT NULL,
  `SellerID` int(11) DEFAULT NULL,
  `SellerOrderReference` varchar(30) DEFAULT NULL,
  `BankAccountID` int(11) DEFAULT NULL,
  `InsuranceID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ContractID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Table structure for table `insurances`
--

CREATE TABLE IF NOT EXISTS `insurances` (
  `InsuranceID` int(11) NOT NULL AUTO_INCREMENT,
  `InsuranceContractNumber` varchar(60) CHARACTER SET utf8 NOT NULL,
  `ProductTypeID` int(11) NOT NULL,
  PRIMARY KEY (`InsuranceID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE IF NOT EXISTS `invoices` (
  `InvoiceID` int(11) NOT NULL AUTO_INCREMENT,
  `InvoiceSentDate` date NOT NULL,
  `InvoiceStatus` tinyint(1) NOT NULL,
  `InvoiceDueDate` date NOT NULL,
  PRIMARY KEY (`InvoiceID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `producttypes`
--

CREATE TABLE IF NOT EXISTS `producttypes` (
  `ProductTypeID` int(11) NOT NULL AUTO_INCREMENT,
  `ProductName` varchar(255) NOT NULL,
  `ProductURL` text CHARACTER SET ascii,
  `ProductTerms` text CHARACTER SET ascii,
  PRIMARY KEY (`ProductTypeID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `sellers`
--

CREATE TABLE IF NOT EXISTS `sellers` (
  `SellerID` int(11) NOT NULL AUTO_INCREMENT,
  `SellerName` varchar(255) CHARACTER SET utf8 NOT NULL,
  `SellerEmail` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Address` varchar(100) NOT NULL,
  `ZipCode` varchar(7) NOT NULL,
  `City` varchar(40) NOT NULL,
  `Lat` decimal(10,8) NOT NULL,
  `Lng` decimal(11,8) NOT NULL,
  `SellerBankAccount` varchar(60) NOT NULL,
  `Blocked` tinyint(1) NOT NULL,
  PRIMARY KEY (`SellerID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE IF NOT EXISTS `transactions` (
  `TransactionID` int(11) NOT NULL AUTO_INCREMENT,
  `TransactionDateTime` datetime DEFAULT NULL,
  `InitiatedByBuyerID` int(11) NOT NULL,
  `ContractID` int(11) NOT NULL,
  `TransactionAmount` float NOT NULL,
  `TransactionType` varchar(25) NOT NULL,
  PRIMARY KEY (`TransactionID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `UserID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `HashedPassword` varchar(40) CHARACTER SET ascii NOT NULL,
  `UserType` tinyint(1) NOT NULL,
  `Address` varchar(100) NOT NULL,
  `ZipCode` varchar(7) NOT NULL,
  `City` varchar(40) NOT NULL,
  `Lat` decimal(10,8) NOT NULL,
  `Lng` decimal(11,8) NOT NULL,
  `LogoURL` varchar(100) NOT NULL,
  `BankAccount` varchar(60) DEFAULT NULL,
  `Blocked` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`UserID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=35 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
