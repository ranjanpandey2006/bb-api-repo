package com.bb.service;

import com.bb.entity.User;

public interface IMainService {
	
	public User login(User user);
	
	public User register(User user);
}
