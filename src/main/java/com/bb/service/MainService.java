package com.bb.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bb.dao.IMainDAO;
import com.bb.entity.User;
import com.bb.service.mail.SendMailSSL;
import com.bb.utils.Utils;

@Service
public class MainService implements IMainService {
	
	@Autowired
	private IMainDAO mainDAO;
	
	@Autowired
	private Utils utils;
	
	@Override
	public User login(User user) {
		return mainDAO.login(user);
	}

	@Override
	public User register(User user) {
		
		if(user.getUserType().equals("1")){
			utils.prepareBalanceAndAccount();
		}
		user = mainDAO.register(user);
		user.setPassword("");
		SendMailSSL mail = new SendMailSSL();
		String subject = "Registration Successful for GreenPay";
		String text = "Dear " + user.getName()+", " +
				"\n\nYou have successfully registered." +
				"\n\n\n\n Thanks,"+
				"\n Green Pay Team";
		mail.sendEmail(user.getEmailId(), subject, text);
		return user;
	}

	
}
