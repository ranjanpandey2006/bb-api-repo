package com.bb.service.mail;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class SendMailSSL {
	
	public void sendEmail(String toEmail, String subject,String text){
	
	Properties props = new Properties();
	props.put("mail.smtp.host", "smtp.gmail.com");
	props.put("mail.smtp.socketFactory.port", "465");
	props.put("mail.smtp.socketFactory.class",
			"javax.net.ssl.SSLSocketFactory");
	props.put("mail.smtp.auth", "true");
	props.put("mail.smtp.port", "465");

	Session session = Session.getInstance(props,
		new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("greenpay2018@gmail.com","Hackathon@8");
				//return new PasswordAuthentication("info@green-pay.nl","r3r3r3r3r3");
			}
		});

	try {

		Message message = new MimeMessage(session);
		message.setFrom(new InternetAddress("greenpay2018@gmail.com"));
		message.setRecipients(Message.RecipientType.TO,
				InternetAddress.parse(toEmail));
		message.setSubject(subject);
		message.setText(text);

		Transport.send(message);


	} catch (MessagingException e) {
		throw new RuntimeException(e);
	}
	
	}

}
