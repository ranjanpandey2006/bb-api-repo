package com.bb.service;

import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bb.dao.IContractDAO;
import com.bb.dao.IMainDAO;
import com.bb.entity.BankAccounts;
import com.bb.entity.Contract;
import com.bb.entity.User;
import com.bb.entity.vo.ContractVO;
import com.bb.entity.vo.TransactionVO;
import com.bb.openapi.AccountRouting;
import com.bb.openapi.AccountService;
import com.bb.openapi.AccountVO;
import com.bb.openapi.BalanceVO;
import com.bb.service.mail.SendMailSSL;
import com.bb.utils.Converters;
import com.bb.utils.Utils;

@Service
public class ContractService implements IContractService{
	
	@Autowired
	private IContractDAO contractDAO;
	
	@Autowired
	private IMainDAO mainDAO;
	
	@Autowired
	private Utils utils;
	
	@Override
	public Contract createContract(ContractVO contractVO) {
		Contract contract = Converters.convertContractVOtoDO(contractVO);
		try{
			
			//Logic for get a bank account for Blocked account - API call
	
			String bankAccountNumber = utils.prepareBalanceAndAccount();
			//Logic for creation of insurance id
			contract.setInsuranceId(111222);
			//Get the buyer id
			User user = mainDAO.validEmail(contractVO);
			contract.setBuyerId(user.getUserId());
			//Update the bankAccount table with the OriginalAmount from contactVO
			BankAccounts bankAccount = new BankAccounts();
			bankAccount.setBankAccountNumber(bankAccountNumber);
			bankAccount.setOriginalAmount(contractVO.getContractAmount());
			bankAccount.setRemainingAmount(0);
			bankAccount.setProductId(1);
			
			bankAccount = mainDAO.updateBankAccount(bankAccount);
			contract.setBankAccountId(bankAccount.getBankAccountId());
			//Initiated -Set contract status 0
			contract.setContractStatus(0);			
			contract = contractDAO.createContract(contract);
			sendMail(contractVO,bankAccountNumber);
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		return contract;
	}

	@Override
	public List<Contract> getContracts(Integer userId) {

		List<Contract> listOfContracts = null;
		
		try{
			listOfContracts = contractDAO.getContracts(userId);
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		return listOfContracts;
		
	}
	
	@Override
	public void transfer(TransactionVO transactionVO) {

		try{
			contractDAO.transfer(transactionVO);
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
	}

	@Override
	public Contract getContractDetail(Integer contractId) {

		Contract contract = null;
		
		try{
			contract =  contractDAO.getContractDetail(contractId);
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		return contract;
	}
	
	public void sendMail(ContractVO contractVO, String bankAccountNumber){
		
		try{
		User user = mainDAO.getUser(contractVO.getSellerId());
		SendMailSSL mail = new SendMailSSL();
		
		float valueBackInsuranceFee = 10;
		float totalAmount = valueBackInsuranceFee + contractVO.getContractAmount();
		
		//Send mail to buyer
		String subject = "GreenPay account created by "+user.getName()+".";
		String text = "Dear " + contractVO.getBuyerName()+",\n\n" +
				"Your GreenPay account has been created by the seller ("+ user.getName() +") for an amount of EUR "+contractVO.getContractAmount()+".\n" +
				"The ValueBack Insurance fee is EUR "+valueBackInsuranceFee+".\n\n" +
				"We kindly request you to transfer in total EUR "+totalAmount+".\n\n" +
				"Click on below link to make the payment.\n\n"+
				"<a href='http://www.green-pay.nl/Robabank/make_payment.html?to="+bankAccountNumber+"&amount="+totalAmount+"'>Transfer money to your GreenPay account now</a>\n\n"+
				"As soon as the payment has been done, seller "+user.getName()+" will ship your order immediately.\n\n"+
				"\n\n\n\nThanks,\n\n"+
				"GreenPay: the GREEN way to pay !";
		mail.sendEmail(contractVO.getBuyerEmail(), subject, text);
		
		//Send email to seller
		subject = "GreenPay account created successfully";
		text = "Dear " + user.getName() + ",\n\n" +
				"The GreenPay account has been created successfully. You will be notified when the buyer has made a payment into the account and you can start shipping." +
				"\n\n\n\n Thanks,"+
				"GreenPay: the GREEN way to pay !";
		mail.sendEmail(user.getEmailId(), subject, text);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
	}

	public IContractDAO getContractDAO() {
		return contractDAO;
	}

	public void setContractDAO(IContractDAO contractDAO) {
		this.contractDAO = contractDAO;
	}


}
