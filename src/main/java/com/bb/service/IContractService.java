package com.bb.service;

import java.util.List;

import com.bb.entity.Contract;
import com.bb.entity.vo.ContractVO;
import com.bb.entity.vo.TransactionVO;

public interface IContractService {
	
	public Contract createContract(ContractVO contractVO);
	public List<Contract> getContracts(Integer userId);
	public Contract getContractDetail(Integer contractId);
	public void transfer(TransactionVO transactionVO);
}
