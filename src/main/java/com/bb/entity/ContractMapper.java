package com.bb.entity;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Date;

import org.springframework.jdbc.core.RowMapper;

public class ContractMapper implements RowMapper<Contract> {

	@Override
	public Contract mapRow(ResultSet row, int rowNum) throws SQLException {
		Contract contract = new Contract();
		contract.setContractId(row.getInt("ContractID"));
		contract.setContractDescription(row.getString("ContractDescription"));
		contract.setSellerOrderReference(row.getString("SellerOrderReference"));
		contract.setBuyerName(row.getString("BuyerName"));
		contract.setSellerName(row.getString("SellerName"));
		contract.setBuyerId(row.getInt("BuyerID"));
		contract.setSellerId(row.getInt("SellerID"));
		contract.setContractStatus(row.getInt("ContractStatus"));
		LocalDate date = row.getDate("ContractStartDateTime").toLocalDate();
		contract.setContractStartDate(date);
		contract.setInsuranceId(row.getInt("InsuranceID"));
		contract.setBankAccountId(row.getInt("BankAccountID"));
		try{
		contract.setOriginalAmt(row.getFloat("OriginalAmount"));
		contract.setRemainingAmt(row.getFloat("AmountRemaining"));
		contract.setJointAccount(row.getString("BankAccountNumber"));
		}catch(Exception e){
			
		}
		return contract;
	}
}
