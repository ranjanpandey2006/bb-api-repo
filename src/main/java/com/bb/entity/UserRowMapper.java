package com.bb.entity;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class UserRowMapper implements RowMapper<User> {

	@Override
	public User mapRow(ResultSet row, int rowNum) throws SQLException {
		User user = new User();
		user.setUserId(row.getInt("UserID"));
		user.setName(row.getString("Name"));
		user.setPassword(row.getString("HashedPassword"));
		user.setEmailId(row.getString("Email"));
		user.setUserType(row.getString("UserType"));
		user.setAddress(row.getString("Address"));
		user.setZipCode(row.getString("ZipCode"));
		user.setCity(row.getString("City"));
		user.setLat(row.getLong("Lat"));
		user.setLng(row.getLong("Lng"));
		user.setLogoUrl(row.getString("LogoURL"));
		user.setBankAccount(row.getString("BankAccount"));
		user.setBlocked(row.getBoolean("Blocked"));
		return user;
	}
}