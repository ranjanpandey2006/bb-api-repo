package com.bb.entity;

public class BankAccounts {
	
	private Integer bankAccountId;
	private String bankAccountNumber;
	private Integer productId;
	private float originalAmount;
	private float remainingAmount;
	
	public Integer getBankAccountId() {
		return bankAccountId;
	}
	public void setBankAccountId(Integer bankAccountId) {
		this.bankAccountId = bankAccountId;
	}
	public String getBankAccountNumber() {
		return bankAccountNumber;
	}
	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	public float getOriginalAmount() {
		return originalAmount;
	}
	public void setOriginalAmount(float originalAmount) {
		this.originalAmount = originalAmount;
	}
	public float getRemainingAmount() {
		return remainingAmount;
	}
	public void setRemainingAmount(float remainingAmount) {
		this.remainingAmount = remainingAmount;
	}
	
	

}
