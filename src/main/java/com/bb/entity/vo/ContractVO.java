package com.bb.entity.vo;

public class ContractVO {

	private Integer sellerId;
	private String buyerEmail;
	private String SellerOrderReference;
	private float contractAmount;
	private String contractDescription;
	private String buyerName;
	private String sellerName;
	
	public String getBuyerName() {
		return buyerName;
	}
	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName;
	}
	public String getSellerName() {
		return sellerName;
	}
	public void setSellerName(String sellerName) {
		this.sellerName = sellerName;
	}
	public String getBuyerEmail() {
		return buyerEmail;
	}
	public void setBuyerEmail(String buyerEmail) {
		this.buyerEmail = buyerEmail;
	}
	public String getSellerOrderReference() {
		return SellerOrderReference;
	}
	public void setSellerOrderReference(String sellerOrderReference) {
		SellerOrderReference = sellerOrderReference;
	}
	public float getContractAmount() {
		return contractAmount;
	}
	public void setContractAmount(float contractAmount) {
		this.contractAmount = contractAmount;
	}
	public Integer getSellerId() {
		return sellerId;
	}
	public void setSellerId(Integer sellerId) {
		this.sellerId = sellerId;
	}
	public String getContractDescription() {
		return contractDescription;
	}
	public void setContractDescription(String contractDescription) {
		this.contractDescription = contractDescription;
	}
	
	
}
