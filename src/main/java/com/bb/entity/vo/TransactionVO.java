package com.bb.entity.vo;

import com.bb.openapi.BalanceVO;

public class TransactionVO {

	private ToVO to;
	private BalanceVO value;
	private String description;
	
	public ToVO getTo() {
		return to;
	}
	public void setTo(ToVO to) {
		this.to = to;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public BalanceVO getValue() {
		return value;
	}
	public void setValue(BalanceVO value) {
		this.value = value;
	}
	
}
