package com.bb.entity;

import java.time.LocalDate;
import java.util.Date;

public class Contract {

	private Integer contractId;
	private String contractDescription;
	private String sellerOrderReference;
	private Integer contractStatus;
	private Integer buyerId;
	private Integer sellerId;
	private Integer insuranceId;
	private LocalDate contractStartDate;
	private Integer bankAccountId;
	private String buyerName;
	private String sellerName;
	private Float originalAmt;
	private Float remainingAmt;
	private String jointAccount;
	
	
	public Float getOriginalAmt() {
		return originalAmt;
	}
	public void setOriginalAmt(Float originalAmt) {
		this.originalAmt = originalAmt;
	}
	public Float getRemainingAmt() {
		return remainingAmt;
	}
	public void setRemainingAmt(Float remainingAmt) {
		this.remainingAmt = remainingAmt;
	}
	public String getJointAccount() {
		return jointAccount;
	}
	public void setJointAccount(String jointAccount) {
		this.jointAccount = jointAccount;
	}
	public Integer getContractId() {
		return contractId;
	}
	public void setContractId(Integer contractId) {
		this.contractId = contractId;
	}
	
	public String getContractDescription() {
		return contractDescription;
	}
	public void setContractDescription(String contractDescription) {
		this.contractDescription = contractDescription;
	}
	public String getSellerOrderReference() {
		return sellerOrderReference;
	}
	public void setSellerOrderReference(String sellerOrderReference) {
		this.sellerOrderReference = sellerOrderReference;
	}
	public Integer getContractStatus() {
		return contractStatus;
	}
	public void setContractStatus(Integer contractStatus) {
		this.contractStatus = contractStatus;
	}
	public Integer getBuyerId() {
		return buyerId;
	}
	public void setBuyerId(Integer buyerId) {
		this.buyerId = buyerId;
	}
	public Integer getSellerId() {
		return sellerId;
	}
	public void setSellerId(Integer sellerId) {
		this.sellerId = sellerId;
	}
	public Integer getInsuranceId() {
		return insuranceId;
	}
	public void setInsuranceId(Integer insuranceId) {
		this.insuranceId = insuranceId;
	}
	public LocalDate getContractStartDate() {
		return contractStartDate;
	}
	public void setContractStartDate(LocalDate date) {
		this.contractStartDate = date;
	}
	public Integer getBankAccountId() {
		return bankAccountId;
	}
	public void setBankAccountId(Integer bankAccountId) {
		this.bankAccountId = bankAccountId;
	}
	public String getBuyerName() {
		return buyerName;
	}
	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName;
	}
	public String getSellerName() {
		return sellerName;
	}
	public void setSellerName(String sellerName) {
		this.sellerName = sellerName;
	}
	
	
}
