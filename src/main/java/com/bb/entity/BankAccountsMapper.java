package com.bb.entity;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class BankAccountsMapper implements RowMapper<BankAccounts> {

	@Override
	public BankAccounts mapRow(ResultSet row, int rowNum) throws SQLException {
		BankAccounts accounts = new BankAccounts();
		accounts.setBankAccountId(row.getInt("BankAccountID"));
		accounts.setBankAccountNumber(row.getString("BankAccountNumber"));
		accounts.setOriginalAmount(row.getFloat("OriginalAmount"));
		accounts.setRemainingAmount(row.getFloat("AmountRemaining"));
		accounts.setProductId(row.getInt("ProductTypeID"));
		return accounts;
	}

}
