package com.bb.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.util.UriComponentsBuilder;

import com.bb.entity.User;
import com.bb.service.IMainService;

@Controller
public class UserController {
	
	@Autowired
	private IMainService service;
	
	@PostMapping("login")
	public ResponseEntity<User> login(@RequestBody User user, UriComponentsBuilder builder) {
        user = service.login(user);
        if (null != user && Integer.valueOf(user.getUserId()) != null) {
        	return new ResponseEntity<User>(user, HttpStatus.OK);
        }
        return new ResponseEntity<User>(HttpStatus.UNAUTHORIZED);
	}
	
	@PostMapping("register")
	public ResponseEntity<Integer> register(@RequestBody User user, UriComponentsBuilder builder) {
        user = service.register(user);
        if (Integer.valueOf(user.getUserId()) != null) {
        	return new ResponseEntity<Integer>(user.getUserId(), HttpStatus.OK);
        }
        return new ResponseEntity<Integer>(HttpStatus.FAILED_DEPENDENCY);
	}

}
