package com.bb.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.bb.entity.Contract;
import com.bb.entity.vo.ContractVO;
import com.bb.entity.vo.TransactionVO;
import com.bb.openapi.AccountService;
import com.bb.service.IContractService;

@RestController
public class ContractController {

	@Autowired
	private IContractService contractService;
	
	@Autowired
	private AccountService accountService;
	
	@PostMapping(value = "transfer", consumes = "application/json" , produces = "application/json")
	public void transfer(@RequestBody TransactionVO transactionVO, @RequestParam("from") String from){
		accountService.transfer(transactionVO, from);
		contractService.transfer(transactionVO);
	}
	
	@GetMapping("contracts")
	public ResponseEntity<List<Contract>> contracts(@RequestParam("userId") Integer userId) {
		
		//accountService.createBankAccount(accountVO);

		//List<Account> accounts = accountService.fetchPrivateAccounts(true);
		
		//System.out.println("Accounts == "+accounts);
		
		List<Contract> listOfContract = contractService.getContracts(userId);
		
		if (listOfContract != null) {
        	return new ResponseEntity<List<Contract>>(listOfContract, HttpStatus.OK);
        }
		
        return new ResponseEntity<List<Contract>>(HttpStatus.NOT_FOUND);
	}
	
	@GetMapping("contracts/{contractId}")
	public ResponseEntity<Contract> getContract(@PathVariable Integer contractId) {

		Contract contract = contractService.getContractDetail(contractId);
		
		if (contract != null) {
        	return new ResponseEntity<Contract>(contract, HttpStatus.OK);
        }
		
        return new ResponseEntity<Contract>(HttpStatus.NOT_FOUND);
	}
	
	@PostMapping(value = "contracts", consumes = "application/json" , produces = "application/json")
	public ResponseEntity<Contract> createContract(@RequestBody ContractVO contractVO, UriComponentsBuilder builder) {
		
		Contract contract = contractService.createContract(contractVO);
		return new ResponseEntity<Contract>(contract, HttpStatus.CREATED);
       
	}

	public IContractService getContractService() {
		return contractService;
	}

	public void setContractService(IContractService contractService) {
		this.contractService = contractService;
	}

	public AccountService getAccountService() {
		return accountService;
	}

	public void setAccountService(AccountService accountService) {
		this.accountService = accountService;
	}
	
}
