package com.bb.dao;

import java.util.List;

import com.bb.entity.Contract;
import com.bb.entity.vo.TransactionVO;

public interface IContractDAO {

	public Contract createContract(Contract contract);
	public List<Contract> getContracts(Integer userId);
	public Contract getContractDetail(Integer contractId);
	public void transfer(TransactionVO transactionVO);
	
}
