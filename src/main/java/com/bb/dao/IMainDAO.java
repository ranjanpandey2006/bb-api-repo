package com.bb.dao;

import com.bb.entity.BankAccounts;
import com.bb.entity.User;
import com.bb.entity.vo.ContractVO;

public interface IMainDAO {
	
	public User login(User user);
	public User register(User user);
	
	public User validEmail(ContractVO contractVO);
	public BankAccounts updateBankAccount(BankAccounts bankAccount);
	public User getUser(Integer userId);

}
