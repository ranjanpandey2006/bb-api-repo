package com.bb.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bb.entity.BankAccounts;
import com.bb.entity.BankAccountsMapper;
import com.bb.entity.User;
import com.bb.entity.UserRowMapper;
import com.bb.entity.vo.ContractVO;

@Transactional
@Repository
public class MainDAO implements IMainDAO {

	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	@Override
	public User login(User user) {
		String sql = "SELECT * FROM users WHERE Email = ? and HashedPassword = ?";
		RowMapper<User> rowMapper = new BeanPropertyRowMapper<User>(User.class);
		user = (User) jdbcTemplate.queryForObject(sql, rowMapper, user.getEmailId(), user.getPassword());
		return user;
	}

	@Override
	public User register(User user) {
		String sql = "INSERT INTO users (Name, Email, HashedPassword,UserType,Address,ZipCode,City,Lat,Lng,LogoURL,BankAccount) values (?, ?, ?,?, ?, ?,?,?, ?, ?,?)";
		jdbcTemplate.update(sql, user.getName() , user.getEmailId(),user.getPassword(),user.getUserType(),user.getAddress(),user.getZipCode(),user.getCity(),user.getLat(),user.getLng(),user.getLogoUrl(),user.getBankAccount());
		
		sql = "SELECT * FROM users ORDER BY UserID DESC LIMIT 1";
		RowMapper<User> rowMapper = new UserRowMapper();
		user = jdbcTemplate.queryForObject(sql,rowMapper);
		
		return user;
	}
	
	@Override
	public User validEmail(ContractVO contractVO) {
		String sql = "SELECT * FROM users WHERE Email = ?";
		RowMapper<User> rowMapper = new UserRowMapper();
		User user = null;
		try{
		   user = jdbcTemplate.queryForObject(sql, rowMapper, contractVO.getBuyerEmail());
		}catch (Exception e) {
			user = new User();
			user.setName(contractVO.getBuyerName());
			user.setEmailId(contractVO.getBuyerEmail());
			user.setPassword("Welcome@123");
			user.setUserType("2");
			user.setAddress("Terwijde");
			user.setZipCode("3547 DE");
			user.setCity("Utrecht");
			user.setLat(16);
			user.setLng(67);
			user.setLogoUrl("");
			user.setBankAccount("NLABNA0987654321");
			user = register(user);
		}
		
		return user;
	}
	
	@Override
	public User getUser(Integer userId) {
		String sql = "SELECT * FROM users WHERE UserId = ?";
		RowMapper<User> rowMapper = new UserRowMapper();
		User user = (User) jdbcTemplate.queryForObject(sql, rowMapper, userId);
		return user;
	}
	
	@Override
	public BankAccounts updateBankAccount(BankAccounts bankAccount) {
		String bankAcct = bankAccount.getBankAccountNumber();
		String sql = "INSERT INTO bankaccounts (BankAccountNumber, ProductTypeID, OriginalAmount,AmountRemaining) VALUES (?, ?, ?, ?)";
		jdbcTemplate.update(sql,bankAccount.getBankAccountNumber(),bankAccount.getProductId(),bankAccount.getOriginalAmount(),bankAccount.getRemainingAmount());
		
		sql = "SELECT * FROM bankaccounts ORDER BY BankAccountID DESC LIMIT 1";
		RowMapper<BankAccounts> rowMapper = new BankAccountsMapper();
		bankAccount = jdbcTemplate.queryForObject(sql,rowMapper);
		
		return bankAccount;
	}

}
