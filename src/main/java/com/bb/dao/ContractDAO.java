package com.bb.dao;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bb.entity.BankAccounts;
import com.bb.entity.BankAccountsMapper;
import com.bb.entity.Contract;
import com.bb.entity.ContractMapper;
import com.bb.entity.User;
import com.bb.entity.UserRowMapper;
import com.bb.entity.vo.TransactionVO;
import com.bb.service.mail.SendMailSSL;

@Transactional
@Repository
public class ContractDAO implements IContractDAO {

	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	@Override
	public Contract createContract(Contract contract) {
		
		Date startDate = new Date(System.currentTimeMillis());
		String sql = "INSERT INTO contracts (BuyerName,SellerName,ContractDescription,ContractStartDateTime,ContractStatus,BuyerID,SellerID, SellerOrderReference,BankAccountID,InsuranceID) values "
				+ "(?,?,?, ?,?, ?,?, ?,?, ?)";
		jdbcTemplate.update(sql,contract.getBuyerName(), contract.getSellerName() ,contract.getContractDescription(),startDate,contract.getContractStatus()
				,contract.getBuyerId(),contract.getSellerId(), contract.getSellerOrderReference(),contract.getBankAccountId(),
				contract.getInsuranceId());
		
		sql = "SELECT ContractID, ContractDescription, SellerOrderReference FROM contracts ORDER BY ContractID DESC LIMIT 1";
		RowMapper<Contract> rowMapper = new BeanPropertyRowMapper<Contract>(Contract.class);
		contract = jdbcTemplate.queryForObject(sql, rowMapper);
		
		return contract;
		
	}

	@Override
	public List<Contract> getContracts(Integer userId) {
		
		String sql = "SELECT * FROM users WHERE UserID = ?";
		RowMapper<User> rowMapper = new UserRowMapper();
		User user = (User) jdbcTemplate.queryForObject(sql, rowMapper, userId);
		
		if(user != null && user.getUserType().equals("1")){
			sql = "SELECT * FROM contracts "
					+ "INNER JOIN users ON contracts.SellerID = users.UserID WHERE users.UserId = ?";
		}else if(user != null && user.getUserType().equals("2")){
			sql = "SELECT * FROM contracts "
					+ "INNER JOIN users ON contracts.BuyerID = users.UserID WHERE users.UserId = ?";
		}
		
		RowMapper<Contract> rowMapperContract = new ContractMapper();
		return this.jdbcTemplate.query(sql, rowMapperContract,userId);
		
	}
	
	@Override
	public void transfer(TransactionVO transactionVO) {
		
		String sql = "SELECT * FROM bankaccounts WHERE BankAccountNumber = ? ";
		RowMapper<BankAccounts> rowMapper = new BankAccountsMapper();
		BankAccounts bankaccounts = jdbcTemplate.queryForObject(sql, rowMapper, transactionVO.getTo().getAccount_id());
		Float remainingBal = bankaccounts.getOriginalAmount() - Float.parseFloat(transactionVO.getValue().getAmount());
		sql = "Update bankaccounts SET AmountRemaining = ? WHERE BankAccountNumber = ? ";	
		jdbcTemplate.update(sql,remainingBal,transactionVO.getTo().getAccount_id());
		
		//Select the seller
		/*sql = "SELECT * FROM contracts WHERE BankAccountID = ?";
		RowMapper<Contract> contractMapper = new ContractMapper();
		Contract contract = jdbcTemplate.queryForObject(sql, contractMapper, bankaccounts.getBankAccountId());*/
		//Select the user
		/*sql = "SELECT * FROM users WHERE SellerID = ?";
		RowMapper<User> rowMapperUser = new UserRowMapper();
		User user = jdbcTemplate.queryForObject(sql, rowMapperUser,contract.getSellerId());*/
		
		//Add logic for sending mail to seller
		SendMailSSL mail = new SendMailSSL();
		String subject = "Payment Successful"; //for "+contract.getSellerOrderReference();
		String text = "Dear Seller, \n\n" +// user.getName()+",\n\n" +
				"Payment is done successfully! "+//for an amount of"+ remainingBal  +" EUR by the buyer for " + contract.getSellerOrderReference() +
				".Please start the shipment."+
				"\n\n\n\nThanks,\n\n"+
				"GreenPay: the GREEN way to pay !";
		
		mail.sendEmail("karel.lammers@gmail.com",subject, text);
		
	}

	@Override
	public Contract getContractDetail(Integer contractId) {
		
		String sql = "SELECT * FROM contracts c, bankaccounts b WHERE c.ContractID = ? AND b.BankAccountID = c.BankAccountID";
		RowMapper<Contract> rowMapper = new ContractMapper();
		Contract contract = jdbcTemplate.queryForObject(sql, rowMapper, contractId);
		return contract;
		
	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

}
