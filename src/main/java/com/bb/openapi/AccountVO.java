package com.bb.openapi;

public class AccountVO {
	
	private String user_id;
	private String label;
	private String type;
	private BalanceVO balance;
	private AccountRouting account_routing;
	private String branch_id;
	
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public BalanceVO getBalance() {
		return balance;
	}
	public void setBalance(BalanceVO balance) {
		this.balance = balance;
	}
	public AccountRouting getAccount_routing() {
		return account_routing;
	}
	public void setAccount_routing(AccountRouting account_routing) {
		this.account_routing = account_routing;
	}
	public String getBranch_id() {
		return branch_id;
	}
	public void setBranch_id(String branch_id) {
		this.branch_id = branch_id;
	}

}
