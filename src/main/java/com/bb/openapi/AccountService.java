package com.bb.openapi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.bb.entity.vo.TransactionVO;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class AccountService {
	
	@Value("${obp.username}")
    private String username;

    @Value("${obp.password}")
    private String password;
	
    @Value("${obp.api.versionedUrl}/my/accounts")
    private String privateAccountUrl;

    @Value("${obp.api.versionedUrl}")
    private String apiUrl;
    
    @Value("${obp.api.rootUrl}")
    private String rootUrl;

    @Autowired
    private DirectAuthenticationService authenticationService;
    
    
    public void createBankAccount(AccountVO account,String accountNumber){
    	
    	String token = authenticationService.login(username, password);
    	String dlHeader = String.format("DirectLogin token=%s", token);
        
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
        headers.add("HeaderName", "value");
        headers.add("Content-Type", "application/json");
        headers.add(HttpHeaders.AUTHORIZATION, dlHeader);

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

        HttpEntity<AccountVO> request = new HttpEntity<AccountVO>(account, headers);
        restTemplate.put(apiUrl+"/banks/bb.03.nl.nl/accounts/"+accountNumber, request);
        
    }
    
    public void transfer(TransactionVO transactionVO, String from){
    	
    	String token = authenticationService.login(username, password);
    	String dlHeader = String.format("DirectLogin token=%s", token);
        
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
        headers.add("HeaderName", "value");
        headers.add("Content-Type", "application/json");
        headers.add(HttpHeaders.AUTHORIZATION, dlHeader);

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

        HttpEntity<TransactionVO> request = new HttpEntity<TransactionVO>(transactionVO, headers);
        restTemplate.postForObject(apiUrl+"/banks/bb.03.nl.nl/accounts/"+from+"/owner/transaction-request-types/SANDBOX_TAN/transaction-requests", request, Object.class);
        
    }

}
