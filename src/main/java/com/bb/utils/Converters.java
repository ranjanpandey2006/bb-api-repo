package com.bb.utils;

import com.bb.entity.Contract;
import com.bb.entity.vo.ContractVO;

public class Converters {

	public static Contract convertContractVOtoDO(ContractVO vo){
		Contract contract = new Contract();
		contract.setSellerId(vo.getSellerId());
		contract.setSellerOrderReference(vo.getSellerOrderReference());
		contract.setContractDescription(vo.getContractDescription());
		contract.setBuyerName(vo.getBuyerName());
		contract.setSellerName(vo.getSellerName());
		return contract;
	}
}
