package com.bb.utils;

import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bb.openapi.AccountRouting;
import com.bb.openapi.AccountService;
import com.bb.openapi.AccountVO;
import com.bb.openapi.BalanceVO;

@Component
public class Utils {
	
	@Autowired
	private AccountService accountService;
	
	public String prepareBalanceAndAccount(){
		
		//Logic for get a bank account for Blocked account - API call
		
		
		Random rand = new Random();
		String bankAccountNumber = "NLABNA0"+rand.nextInt(999999999);
		
		AccountVO accountVO = new AccountVO();
		accountVO.setUser_id("7330bed8-31dd-464f-a417-7813e1eb5be4");
		accountVO.setLabel("Joint_Buyer_Seller");
		accountVO.setType("JOINT");
		
		BalanceVO balanceVO = new BalanceVO();
		balanceVO.setAmount("0.00");
		balanceVO.setCurrency("EUR");
		accountVO.setBalance(balanceVO);
		accountVO.setBranch_id("8989898");
		
		AccountRouting accountRouting =  new AccountRouting();
		accountRouting.setAddress("Amsterdam 1102BS");
		accountRouting.setScheme("OBP");
		accountVO.setAccount_routing(accountRouting);
		
		accountService.createBankAccount(accountVO, bankAccountNumber);
		return bankAccountNumber;
	}

}
