package com.bb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
@SpringBootApplication
public class BBApplication extends SpringBootServletInitializer{  
	public static void main(String[] args) {
		//https://www.concretepage.com/spring-boot/spring-boot-jdbc-example
		SpringApplication.run(BBApplication.class, args);
    }
	@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(BBApplication.class);
    }
}            