package com.bb.entity;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class BankAccountsTest {
	
	BankAccounts ba;
	
	@Before
	public void setup(){
		ba = new BankAccounts();
	}

	@Test
	public void test_bankAccounts(){
		ba.getBankAccountId();
		ba.setBankAccountId(222);
		ba.getBankAccountNumber();
		ba.setBankAccountNumber("12323");
		ba.getProductId();
		ba.setProductId(2);
		ba.getOriginalAmount();
		ba.setOriginalAmount(546465);
		ba.setRemainingAmount(454);
		ba.getRemainingAmount();
	}
}
