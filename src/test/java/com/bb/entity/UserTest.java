package com.bb.entity;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class UserTest {

    User user;

    @Test
    public void getUserId() {
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getUserId(), 3);
    }

    @Test
    public void setUserId() {
        Assert.assertNotNull(user);
    }

    @Test
    public void getName() {
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getName(), "Alisha");
    }

    @Test
    public void setName() {
        Assert.assertNotNull(user);
    }

    @Test
    public void getPassword() {
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getPassword(), "test");
    }

    @Test
    public void setPassword() {
        Assert.assertNotNull(user);
    }

    @Test
    public void getEmailId() {
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getEmailId(), "a@g.com");
    }

    @Test
    public void setEmailId() {
        Assert.assertNotNull(user);
    }

    @Test
    public void getPhone() {
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getPhone(), "0633423432");
    }

    @Test
    public void setPhone() {
        Assert.assertNotNull(user);
    }

    @Test
    public void getUserType() {
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getUserType(), "1");
    }

    @Test
    public void setUserType() {
        Assert.assertNotNull(user);
    }

    @Test
    public void getAddress() {
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getAddress(), "Ams");
    }

    @Test
    public void setAddress() {
        Assert.assertNotNull(user);
    }

    @Test
    public void getZipCode() {
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getZipCode(), "1142");
    }

    @Test
    public void setZipCode() {
        Assert.assertNotNull(user);
    }

    @Test
    public void getCity() {
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getCity(), "UT");
    }

    @Test
    public void setCity() {
        Assert.assertNotNull(user);
    }

    @Test
    public void getRegion() {
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getRegion(), "North");
    }

    @Test
    public void setRegion() {
        Assert.assertNotNull(user);
    }

    @Test
    public void getLat() {
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getLat(), (long) 23.343);
    }

    @Test
    public void setLat() {
        Assert.assertNotNull(user);
    }

    @Test
    public void getLng() {
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getLng(), (long) 34.45455);
    }

    @Test
    public void setLng() {
        Assert.assertNotNull(user);
    }

    @Test
    public void getLogoUrl() {
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getLogoUrl(), "www.google.com");
    }

    @Test
    public void setLogoUrl() {
        Assert.assertNotNull(user);
    }

    @Test
    public void getBankAccount() {
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getBankAccount(), "NL45454");
    }

    @Test
    public void setBankAccount() {
        Assert.assertNotNull(user);
    }

    @Test
    public void isBlocked() {
        Assert.assertNotNull(user);
        Assert.assertEquals(user.isBlocked(), false);
    }

    @Test
    public void setBlocked() {
        Assert.assertNotNull(user);
    }

    @Before
    public void setUp() throws Exception {
        user = new User();
        user.getAddress();
        user.setAddress("Ams");
        user.getBankAccount();
        user.setBankAccount("NL45454");
        user.getCity();
        user.setCity("UT");
        user.getEmailId();
        user.setEmailId("a@g.com");
        user.getLat();
        user.setLat((long) 23.343);
        user.getLng();
        user.setLng((long) 34.45455);
        user.getLogoUrl();
        user.setLogoUrl("www.google.com");
        user.getName();
        user.setName("Alisha");
        user.getPassword();
        user.setPassword("test");
        user.getPhone();
        user.setPhone("0633423432");
        user.getRegion();
        user.setRegion("North");
        user.getUserId();
        user.setUserId(3);
        user.getUserType();
        user.setUserType("1");
        user.getZipCode();
        user.setZipCode("1142");
    }
}
