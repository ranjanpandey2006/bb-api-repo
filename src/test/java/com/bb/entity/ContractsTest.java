package com.bb.entity;

import java.time.LocalDate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class ContractsTest {

	Contract c;
	
	@Test
	public void test_contract(){
		c = new Contract();
		c.getBankAccountId();
		c.setBankAccountId(444);
		c.getBuyerId();
		c.setBuyerId(11);
		c.getContractDescription();
		c.setContractDescription("Test");
		c.getContractId();
		c.setContractId(5656);
		c.getContractStartDate();
		c.setContractStartDate(LocalDate.now());
		c.getContractStatus();
		c.setContractStatus(0);
		c.getInsuranceId();
		c.setInsuranceId(76789);
		c.getSellerId();
		c.setSellerId(41);
		c.getSellerOrderReference();
		c.setSellerOrderReference("test ref");
	}
	
}
