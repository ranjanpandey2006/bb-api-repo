package com.bb.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.bb.dao.IContractDAO;
import com.bb.dao.IMainDAO;
import com.bb.entity.Contract;
import com.bb.entity.User;
import com.bb.entity.vo.ContractVO;
import com.bb.openapi.AccountVO;
import com.bb.service.mail.SendMailSSL;
import com.bb.utils.Utils;

@RunWith(SpringRunner.class)
public class ContractServiceTest {
	
	@TestConfiguration
    static class ContractServiceTestContextConfiguration {
  
        @Bean
        public IContractService contractService() {
            return new ContractService();
        }
    }
	
	@Autowired
	private IContractService contractService;
	
	@MockBean
	private IContractDAO contractDAO;
	
	@MockBean
	private IMainDAO mainDAO;
	
	@MockBean
	private Utils utils;
	

	@Before
	public void setup(){
		
	}
	
	@Test
	public void test_getContracts(){
		List<Contract> list = new ArrayList<>();
		Integer userId = 1;
		Contract c = new Contract();
		c.setContractId(22);
		list.add(c);
		Mockito.when(contractDAO.getContracts(userId)).thenReturn(list);
		List<Contract> contracts = contractService.getContracts(userId);
		
		assertEquals("List has 1 contract", 1, contracts.size());
		assertEquals("Object 1 has contract id as 22","22",contracts.get(0).getContractId().toString());
	}
	
	@Test
	public void test_getContractDetail(){
		Integer contractId = 1;
		Contract c = new Contract();
		c.setContractId(22);
		Mockito.when(contractDAO.getContractDetail(contractId)).thenReturn(c);
		Contract contract = contractService.getContractDetail(contractId);
		
		assertEquals("Object 1 has contract id as 22","22",c.getContractId().toString());
	}
	
	@Test
	public void test_createContract(){
		
		User user = new User();
		user.setUserId(22);
		ContractVO contractVO = new ContractVO();
		contractVO.setBuyerEmail("buyer@g.com");
		contractVO.setSellerOrderReference("BUYER");
		Integer contractId = 1;
		Contract c = new Contract();
		c.setContractId(22);
		AccountVO acctVO = new AccountVO();
		SendMailSSL mail = mock(SendMailSSL.class);
		Mockito.when(contractDAO.getContractDetail(contractId)).thenReturn(c);
	//	Mockito.when(mainDAO.validEmail(new String())).thenReturn(user);
		Mockito.when(utils.prepareBalanceAndAccount()).thenReturn("1234");
		Mockito.when(mainDAO.getUser(1)).thenReturn(user);
		//doNothing().when(acctService).createBankAccount(acctVO, new String());
		doNothing().when(mail).sendEmail(user.getEmailId(), "Test", "Hi");
		Contract contract = contractService.createContract(contractVO);
		
		assertEquals("Object 1 has contract id as 22","22",c.getContractId().toString());
		assertTrue("Contract object", null != contract);
	}
	
}
