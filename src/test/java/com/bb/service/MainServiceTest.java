package com.bb.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.bb.dao.IMainDAO;
import com.bb.entity.User;
import com.bb.openapi.AccountService;
import com.bb.openapi.AccountVO;
import com.bb.service.mail.SendMailSSL;
import com.bb.utils.Utils;

@RunWith(SpringRunner.class)
public class MainServiceTest {

	@TestConfiguration
    static class MainServiceTestContextConfiguration {
  
        @Bean
        public IMainService mainService() {
            return new MainService();
        }
    }
	@Autowired
	private IMainService mainService;
	
	@MockBean
	private IMainDAO mainDAO;
	
	@MockBean
	private Utils utils;
	
	User user;
	
	@Before
	public void setup(){
		user = new User();
		user.setUserId(1);
		user.setUserType("1");
		user.setEmailId("a@g.com");
	}
	
	@Test
	public void test_login(){
		
		Mockito.when(mainDAO.login(user)).thenReturn(user);
		User found = mainService.login(user);
		assertTrue("User object is not null", null!=found);
		assertEquals("User id is 1", 1, found.getUserId());
	}
	
	@Test
	public void test_register(){
		SendMailSSL mail = mock(SendMailSSL.class);
		Mockito.when(mainDAO.register(user)).thenReturn(user);
		Mockito.when(utils.prepareBalanceAndAccount()).thenReturn("1234");
		doNothing().when(mail).sendEmail(user.getEmailId(), "Test", "Hi");
		
		User registration = mainService.register(user);
		
		assertTrue("Úser object is not null", null != registration);
	}
}
